from __future__ import annotations

import uuid
from datetime import date, datetime
from uuid import UUID

from sqlalchemy import ForeignKey, UniqueConstraint
from sqlalchemy.orm import Mapped, MappedAsDataclass, mapped_column, relationship

from app.db import Base
from lib.time import utc_now


class SpreadsheetImport(MappedAsDataclass, Base, kw_only=True):
    __tablename__ = "spreadsheet_import"

    id: Mapped[UUID] = mapped_column(
        primary_key=True,
        default_factory=uuid.uuid4,
        init=False,
    )
    version: Mapped[int] = mapped_column(unique=True)
    created_at: Mapped[datetime] = mapped_column(default_factory=utc_now)

    projects: Mapped[list[SpreadsheetImportProject]] = relationship()


class SpreadsheetImportProject(MappedAsDataclass, Base):
    __table_args__ = (UniqueConstraint("import_id", "code", "name"),)
    __tablename__ = "spreadsheet_import_project"

    id: Mapped[UUID] = mapped_column(
        primary_key=True,
        default_factory=uuid.uuid4,
        init=False,
    )
    import_id: Mapped[UUID] = mapped_column(
        ForeignKey("spreadsheet_import.id"),
        init=False,
    )
    code: Mapped[int]
    name: Mapped[str]

    entries: Mapped[list[SpreadsheetImportEntry]] = relationship()


class SpreadsheetImportEntry(MappedAsDataclass, Base):
    __tablename__ = "spreadsheet_import_entry"

    id: Mapped[UUID] = mapped_column(
        primary_key=True,
        default_factory=uuid.uuid4,
        init=False,
    )
    project_id: Mapped[UUID] = mapped_column(
        ForeignKey("spreadsheet_import_project.id"),
        init=False,
    )

    date_: Mapped[date] = mapped_column("date")
    fact: Mapped[float | None]
    plan: Mapped[float | None]
