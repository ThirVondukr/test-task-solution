import itertools
from collections.abc import Sequence

from openpyxl import Workbook
from openpyxl.worksheet.worksheet import Worksheet

from app.db.models import SpreadsheetImport, SpreadsheetImportEntry


def _sorted_entries(
    entries: Sequence[SpreadsheetImportEntry],
) -> Sequence[SpreadsheetImportEntry]:
    return sorted(entries, key=lambda e: e.date_)


def _set_up_headers(sheet: Worksheet, spreadsheet: SpreadsheetImport) -> None:
    entries = spreadsheet.projects[0].entries if spreadsheet.projects else []
    header: list[object] = [
        None,
        None,
        *itertools.chain.from_iterable(
            (entry.date_, None) for entry in _sorted_entries(entries)
        ),
    ]
    sheet.append(header)
    for i in range(3, len(header) + 1, 2):
        sheet.merge_cells(start_row=1, end_row=1, start_column=i, end_column=i + 1)

    header = [
        "Код",
        "Наименование проекта",
    ]
    for _ in entries:
        header.append("план")
        header.append("факт")
    sheet.append(header)


def export_spreadsheet(spreadsheet: SpreadsheetImport) -> Workbook:
    wb = Workbook()
    if wb.active:
        wb.remove(wb.active)  # type: ignore[arg-type]
    sheet: Worksheet = wb.create_sheet(title="data")

    _set_up_headers(sheet=sheet, spreadsheet=spreadsheet)

    for project in spreadsheet.projects:
        row = [project.code, project.name]
        for entry in _sorted_entries(project.entries):
            row.append(entry.plan)
            row.append(entry.fact)

        sheet.append(row)

    return wb
