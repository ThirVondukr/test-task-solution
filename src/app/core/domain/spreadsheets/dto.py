import dataclasses
from typing import BinaryIO


@dataclasses.dataclass(frozen=True, slots=True)
class SpreadsheetImportDTO:
    io: BinaryIO
