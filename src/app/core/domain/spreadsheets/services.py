from app.core.domain.spreadsheets.repositories import SpreadsheetRepository
from app.db.models import (
    SpreadsheetImport,
    SpreadsheetImportEntry,
    SpreadsheetImportProject,
)
from lib.db import DBContext
from lib.spreadsheets.datastructures import Spreadsheet


class SpreadsheetService:
    def __init__(
        self,
        repository: SpreadsheetRepository,
        db_context: DBContext,
    ) -> None:
        self._repository = repository
        self._db_context = db_context

    async def create(
        self,
        spreadsheet: Spreadsheet,
    ) -> SpreadsheetImport:
        model = SpreadsheetImport(
            version=await self._repository.latest_version(),
            projects=[
                SpreadsheetImportProject(
                    code=project_entry.code,
                    name=project_entry.project_name,
                    entries=[
                        SpreadsheetImportEntry(
                            date_=date_,
                            fact=entry.fact,
                            plan=entry.plan,
                        )
                        for date_, entry in project_entry.entries.items()
                    ],
                )
                for project_entry in spreadsheet.entries
            ],
        )
        self._db_context.add(model)
        await self._db_context.flush()
        return model
