from result import Err, Ok, Result

from app.core.domain.spreadsheets.dto import SpreadsheetImportDTO
from app.core.domain.spreadsheets.services import SpreadsheetService
from app.db.models import SpreadsheetImport
from lib.spreadsheets.errors import InvalidSpreadsheetError
from lib.spreadsheets.parsing import parse_spreadsheet


class SpreadsheetImportCommand:
    def __init__(self, service: SpreadsheetService) -> None:
        self._service = service

    async def execute(
        self,
        dto: SpreadsheetImportDTO,
    ) -> Result[SpreadsheetImport, InvalidSpreadsheetError]:
        data = parse_spreadsheet(io=dto.io)
        if isinstance(data, Err):
            return data
        spreadsheet = await self._service.create(spreadsheet=data.ok_value)
        return Ok(spreadsheet)
