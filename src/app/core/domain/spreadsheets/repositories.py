from collections.abc import Sequence
from uuid import UUID

from sqlalchemy import Select, func, select
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.orm.interfaces import ORMOption

from app.db.models import SpreadsheetImport


class SpreadsheetRepository:
    def __init__(self, session: AsyncSession) -> None:
        self._session = session

    async def latest_version(self) -> int:
        stmt: Select[tuple[int]] = select(func.max(SpreadsheetImport.version))
        return await self._session.scalar(stmt) or 0

    async def get(
        self,
        id_: UUID,
        options: Sequence[ORMOption] = (),
    ) -> SpreadsheetImport | None:
        return await self._session.get(
            SpreadsheetImport,
            ident=id_,
            options=options,
        )
