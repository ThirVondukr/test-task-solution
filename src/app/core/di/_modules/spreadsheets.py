import aioinject

from app.core.domain.spreadsheets.commands import SpreadsheetImportCommand
from app.core.domain.spreadsheets.repositories import SpreadsheetRepository
from app.core.domain.spreadsheets.services import SpreadsheetService
from lib.types import Providers

providers: Providers = [
    aioinject.Callable(SpreadsheetRepository),
    aioinject.Callable(SpreadsheetService),
    aioinject.Callable(SpreadsheetImportCommand),
]
