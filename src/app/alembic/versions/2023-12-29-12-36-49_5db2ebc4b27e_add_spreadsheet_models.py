"""
Add spreadsheet models

Revision ID: 5db2ebc4b27e
Revises:
Create Date: 2023-12-29 12:36:49.558763

"""
import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = "5db2ebc4b27e"
down_revision: str | None = None
branch_labels: str | None = None
depends_on: str | None = None


def upgrade() -> None:
    op.create_table(
        "spreadsheet_import",
        sa.Column("id", sa.Uuid(), nullable=False),
        sa.Column("version", sa.Integer(), nullable=False),
        sa.Column("created_at", sa.DateTime(timezone=True), nullable=False),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_spreadsheet_import")),
        sa.UniqueConstraint("version", name=op.f("uq_spreadsheet_import_version")),
    )
    op.create_table(
        "spreadsheet_import_project",
        sa.Column("id", sa.Uuid(), nullable=False),
        sa.Column("import_id", sa.Uuid(), nullable=False),
        sa.Column("code", sa.Integer(), nullable=False),
        sa.Column("name", sa.String(), nullable=False),
        sa.ForeignKeyConstraint(
            ["import_id"],
            ["spreadsheet_import.id"],
            name=op.f("fk_spreadsheet_import_project_import_id_spreadsheet_import"),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_spreadsheet_import_project")),
        sa.UniqueConstraint(
            "import_id",
            "code",
            "name",
            name=op.f("uq_spreadsheet_import_project_import_id"),
        ),
    )
    op.create_table(
        "spreadsheet_import_entry",
        sa.Column("id", sa.Uuid(), nullable=False),
        sa.Column("project_id", sa.Uuid(), nullable=False),
        sa.Column("date", sa.Date(), nullable=False),
        sa.Column("fact", sa.Float(), nullable=True),
        sa.Column("plan", sa.Float(), nullable=True),
        sa.ForeignKeyConstraint(
            ["project_id"],
            ["spreadsheet_import_project.id"],
            name=op.f(
                "fk_spreadsheet_import_entry_project_id_spreadsheet_import_project",
            ),
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_spreadsheet_import_entry")),
    )


def downgrade() -> None:
    op.drop_table("spreadsheet_import_entry")
    op.drop_table("spreadsheet_import_project")
    op.drop_table("spreadsheet_import")
