from http import HTTPStatus
from typing import Annotated

from aioinject import Inject
from aioinject.ext.fastapi import inject
from fastapi import APIRouter, HTTPException, UploadFile
from result import Err

from app.adapters.api.spreadsheets._schema import SpreadsheetImportResultSchema
from app.core.domain.spreadsheets.commands import SpreadsheetImportCommand
from app.core.domain.spreadsheets.dto import SpreadsheetImportDTO

router = APIRouter()


@router.post("/")
@inject
async def spreadsheet_create(
    file: UploadFile,
    command: Annotated[SpreadsheetImportCommand, Inject],
) -> SpreadsheetImportResultSchema:
    result = await command.execute(
        dto=SpreadsheetImportDTO(
            io=file.file,
        ),
    )
    if isinstance(result, Err):
        raise HTTPException(
            status_code=HTTPStatus.BAD_REQUEST,
            detail=result.err_value.message,
        )

    return SpreadsheetImportResultSchema(id=result.ok_value.id)
