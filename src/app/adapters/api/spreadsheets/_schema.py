from uuid import UUID

from pydantic import ConfigDict

from app.adapters.api.schema import BaseSchema


class SpreadsheetImportResultSchema(BaseSchema):
    model_config = ConfigDict(title="SpreadsheetImportResult")

    id: UUID  # noqa: A003
