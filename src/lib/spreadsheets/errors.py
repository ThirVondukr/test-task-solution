import dataclasses


@dataclasses.dataclass
class InvalidSpreadsheetError:
    message: str
