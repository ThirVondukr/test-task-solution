from collections.abc import Sequence
from datetime import datetime
from typing import BinaryIO
from zipfile import BadZipFile

import openpyxl
from openpyxl.cell import Cell
from openpyxl.worksheet.worksheet import Worksheet
from result import Err, Ok, Result

from lib.itertools import chunked, skip
from lib.spreadsheets._validation import (
    validate_cell_value,
    validate_nullable_cell_value,
)
from lib.spreadsheets.datastructures import MetricEntry, Spreadsheet, SpreadsheetEntry
from lib.spreadsheets.errors import InvalidSpreadsheetError

_HEADER_HEIGHT = 2


def _parse_row(  # noqa: C901
    sheet: Worksheet,
    row: Sequence[Cell],
) -> Result[SpreadsheetEntry, InvalidSpreadsheetError]:
    if isinstance(code := validate_cell_value(row[0], int), Err):
        return code
    if isinstance(project_name := validate_cell_value(row[1], str), Err):
        return project_name

    entries = {}
    for plan_cell, fact_cell in chunked(row[2:], chunk_size=2):
        if isinstance(
            date_ := validate_cell_value(
                sheet.cell(row=1, column=plan_cell.column),
                datetime,
            ),
            Err,
        ):
            return date_
        if isinstance(plan := validate_nullable_cell_value(plan_cell, float), Err):
            return plan
        if isinstance(fact := validate_nullable_cell_value(fact_cell, float), Err):
            return fact

        entries[date_.ok_value.date()] = MetricEntry(
            plan=plan.ok_value,
            fact=fact.ok_value,
        )
    return Ok(
        SpreadsheetEntry(
            code=code.ok_value,
            project_name=project_name.ok_value,
            entries=entries,
        ),
    )


def parse_spreadsheet(io: BinaryIO) -> Result[Spreadsheet, InvalidSpreadsheetError]:
    try:
        workbook = openpyxl.open(io)
    except (BadZipFile, KeyError):
        return Err(InvalidSpreadsheetError("Is not an excel file"))

    try:
        sheet: Worksheet = workbook["data"]
    except KeyError:
        return Err(InvalidSpreadsheetError('Sheet with name "data" does not exist'))

    if len({len(row) for row in sheet.rows}) != 1:
        return Err(InvalidSpreadsheetError(""))

    entries = []
    for row in skip(sheet.rows, 2):
        if isinstance(entry_result := _parse_row(sheet=sheet, row=row), Err):
            return entry_result
        entries.append(entry_result.ok_value)

    return Ok(Spreadsheet(entries=entries))
