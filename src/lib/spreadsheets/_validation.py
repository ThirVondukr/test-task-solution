from typing import TypeVar

import openpyxl.cell
from openpyxl.cell import Cell, MergedCell
from result import Err, Ok, Result

from lib.spreadsheets.errors import InvalidSpreadsheetError

T = TypeVar("T")


def validate_cell_value(
    cell: Cell | MergedCell,
    type_: type[T],
) -> Result[T, InvalidSpreadsheetError]:
    value = cell.value
    if not isinstance(value, type_):
        msg = (
            f"Cell at {cell.coordinate} "
            f"has invalid type {type(value)}, expected {type_}"
        )
        return Err(InvalidSpreadsheetError(msg))
    return Ok(value)


def validate_nullable_cell_value(
    cell: openpyxl.cell.Cell,
    type_: type[T],
) -> Result[T | None, InvalidSpreadsheetError]:
    if cell.value is None:
        return Ok(None)
    return validate_cell_value(cell, type_)
