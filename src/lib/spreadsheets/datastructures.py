from __future__ import annotations

import dataclasses
from collections.abc import Mapping, Sequence
from datetime import date


@dataclasses.dataclass(slots=True, frozen=True)
class Spreadsheet:
    entries: Sequence[SpreadsheetEntry]


@dataclasses.dataclass(slots=True, frozen=True)
class SpreadsheetEntry:
    code: int
    project_name: str
    entries: Mapping[date, MetricEntry]


@dataclasses.dataclass(slots=True, frozen=True)
class MetricEntry:
    plan: float | None
    fact: float | None
