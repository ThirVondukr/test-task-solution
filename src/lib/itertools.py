import itertools
from collections.abc import Iterable, Sequence
from typing import TypeVar

T = TypeVar("T")


def skip(iterable: Iterable[T], n: int) -> Iterable[T]:
    return itertools.islice(iterable, n, None)


def chunked(iterable: Iterable[T], chunk_size: int) -> Iterable[Sequence[T]]:
    chunk = []
    for element in iterable:
        chunk.append(element)
        if len(chunk) >= chunk_size:
            yield chunk
            chunk = []
    if chunk:
        yield chunk
