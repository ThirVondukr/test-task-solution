from typing import Protocol, TypeAlias, TypeVar

from fastapi import FastAPI

T = TypeVar("T")


class DIContext(Protocol):
    async def resolve(self, type_: type[T]) -> T:
        ...


HttpApp: TypeAlias = FastAPI
