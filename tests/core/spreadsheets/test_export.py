from io import BytesIO
from typing import BinaryIO

import pytest

from app.core.domain.spreadsheets.commands import SpreadsheetImportCommand
from app.core.domain.spreadsheets.dto import SpreadsheetImportDTO
from app.core.domain.spreadsheets.export import export_spreadsheet
from lib.spreadsheets.parsing import parse_spreadsheet
from tests.utils import compare_spreadsheet

pytestmark = [pytest.mark.anyio, pytest.mark.usefixtures("session")]


async def test_roundtrip(
    example_spreadsheet_io: BinaryIO,
    spreadsheet_import_command: SpreadsheetImportCommand,
) -> None:
    spreadsheet_result = await spreadsheet_import_command.execute(
        dto=SpreadsheetImportDTO(io=example_spreadsheet_io),
    )
    spreadsheet = spreadsheet_result.unwrap()

    exported_workbook = export_spreadsheet(spreadsheet)
    io = BytesIO()
    exported_workbook.save(io)

    parsed_spreadsheet = parse_spreadsheet(io).unwrap()

    compare_spreadsheet(spreadsheet, parsed_spreadsheet)
