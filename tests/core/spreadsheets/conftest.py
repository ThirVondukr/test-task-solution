import pytest

from app.core.domain.spreadsheets.commands import SpreadsheetImportCommand
from tests.types import DIContext


@pytest.fixture
async def spreadsheet_import_command(di_context: DIContext) -> SpreadsheetImportCommand:
    return await di_context.resolve(SpreadsheetImportCommand)
