from collections.abc import Iterator
from pathlib import Path
from typing import BinaryIO

import pytest


@pytest.fixture
def example_spreadsheet_io() -> Iterator[BinaryIO]:
    with Path("tests/assets/spreadsheet.xlsx").open("rb") as f:
        yield f
