from http import HTTPStatus
from io import BytesIO
from typing import BinaryIO

import httpx
import pytest
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.adapters.api.spreadsheets._schema import SpreadsheetImportResultSchema
from app.db.models import SpreadsheetImport
from tests.types import HttpApp

pytestmark = [pytest.mark.anyio]


@pytest.fixture
async def endpoint_url(http_app: HttpApp) -> str:
    return http_app.url_path_for("spreadsheet_create")


async def test_not_an_excel_file(
    http_client: httpx.AsyncClient,
    endpoint_url: str,
) -> None:
    response = await http_client.post(endpoint_url, files={"file": BytesIO()})
    assert response.status_code == HTTPStatus.BAD_REQUEST


async def test_ok(
    http_client: httpx.AsyncClient,
    endpoint_url: str,
    session: AsyncSession,
    example_spreadsheet_io: BinaryIO,
) -> None:
    response = await http_client.post(
        endpoint_url,
        files={"file": example_spreadsheet_io},
    )
    assert response.status_code == HTTPStatus.OK
    schema = SpreadsheetImportResultSchema.model_validate(response.json())

    spreadsheet = (await session.scalars(select(SpreadsheetImport))).one()
    assert spreadsheet.id == schema.id
