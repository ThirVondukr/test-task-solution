from app.db.models import SpreadsheetImport
from lib.spreadsheets.datastructures import Spreadsheet


def compare_spreadsheet(
    spreadsheet: SpreadsheetImport,
    parsed_spreadsheet: Spreadsheet,
) -> None:
    for project, parsed_project in zip(
        spreadsheet.projects,
        parsed_spreadsheet.entries,
        strict=True,
    ):
        assert project.code == parsed_project.code
        assert project.name == parsed_project.project_name

        for entry, (date_, parsed_entry) in zip(
            project.entries,
            parsed_project.entries.items(),
            strict=True,
        ):
            assert entry.date_ == date_
            assert entry.plan == parsed_entry.plan
            assert entry.fact == parsed_entry.fact
