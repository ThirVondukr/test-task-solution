from collections.abc import AsyncIterator

import dotenv
import httpx
import pytest
from asgi_lifespan import LifespanManager
from fastapi import FastAPI

from app.core.di import create_container
from tests.types import DIContext, HttpApp

dotenv.load_dotenv(".env")

pytest_plugins = [
    "anyio",
    "sqlalchemy_pytest.database",
    "tests.plugins.fixture_typecheck",
    "tests.plugins.services",
    "tests.plugins.database",
    "tests.plugins.spreadsheets",
]


@pytest.fixture(scope="session")
def anyio_backend() -> str:
    return "asyncio"


@pytest.fixture(scope="session")
async def http_app() -> AsyncIterator[FastAPI]:
    from app.adapters.api.app import create_app

    app = create_app()
    async with LifespanManager(app=app):
        yield app


@pytest.fixture
async def http_client(http_app: HttpApp) -> AsyncIterator[httpx.AsyncClient]:
    async with httpx.AsyncClient(
        app=http_app,
        base_url="http://test",
    ) as client:
        yield client


@pytest.fixture(scope="session")
def worker_id() -> str:
    return "main"


@pytest.fixture
async def di_context() -> AsyncIterator[DIContext]:
    async with create_container().context() as ctx:
        yield ctx
